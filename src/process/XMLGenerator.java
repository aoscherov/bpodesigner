package process;

import generated.*;

import javax.xml.bind.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class XMLGenerator {
    private PlatformEntityType platformEntity;
    private UddFile uddFile;
    private final ObjectFactory factory;
    private final PackageFragmentType fragment;

    public static void main(String[] args) throws JAXBException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		ObjectFactory factory = new ObjectFactory();
		
//		PlatformEntityType platformEntity = factory.createPlatformEntityType();
//		platformEntity.setKeyPrefix("820");
//		platformEntity.setName("S2XRecordMap");
//
//		FlexFieldType flexField0 = factory.createFlexFieldType();
//		flexField0.setName("Record");
//		flexField0.setSlot("0");
//		flexField0.setFieldType("FOREIGNKEY");
//		flexField0.setDomain("Contact");
//		flexField0.setForeignKeyConstraint(ForeignKeyConstraintType.CASCADE);
//
//		FlexIndexType flexIndex = factory.createFlexIndexType();
//		flexIndex.setField1Name("Record");
//		flexIndex.setIndexNum(-1);
//
//		platformEntity.getFlexFieldOrFlexFieldListOrFlexIndex().add(flexField0);
//		platformEntity.getFlexFieldOrFlexFieldListOrFlexIndex().add(flexIndex);

        UddFile uddFile = factory.createUddFile();
        PackageFragmentType fragment = factory.createPackageFragmentType();
        PlatformEntityType platformEntity = factory.createPlatformEntityType();
        JAXBElement<PlatformEntityType> platformEntity1 = factory.createAbstractFeatureTypePlatformEntity(platformEntity);
        fragment.getStaticEnumOrDomainSetOrDomainSetExtension().add(platformEntity1);
        uddFile.getIncludeOrCryptoOrPackage().add(fragment);

        platformEntity.setKeyPrefix("255");

		JAXBContext context = JAXBContext.newInstance("generated");
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		marshaller.marshal(uddFile, System.out);

        XMLGenerator generator = new XMLGenerator();
        Map<String, Object> map = new HashMap<>();
        map.put("Name", "SimpleType");
        List<String> list = new ArrayList<>();
        list.add("One");
        list.add("Two");
        generator.addStaticEnum(map, list);
        Map<String, Object> platformMap = new HashMap<>();
        platformMap.put("Name", "Simple");
        platformMap.put("EditAccess", "always");
        generator.generate(platformMap);
	}

    public XMLGenerator() {
        factory = new ObjectFactory();

        uddFile = factory.createUddFile();
        platformEntity = factory.createPlatformEntityType();
        fragment = factory.createPackageFragmentType();
        JAXBElement<PlatformEntityType> platformEntity1 = factory.createAbstractFeatureTypePlatformEntity(platformEntity);
        fragment.getStaticEnumOrDomainSetOrDomainSetExtension().add(platformEntity1);
        uddFile.getIncludeOrCryptoOrPackage().add(fragment);

    }

    public void setAttributes(Map<String, Object> mappings, Object entity) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        mappings.entrySet().stream().forEach(entry -> {
            try {
                String methodName = "set" + entry.getKey();
                Method method = entity.getClass().getMethod(methodName, entry.getValue().getClass());
                method.invoke(entity, entry.getValue());
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }

    public void generate(Map<String, Object> attributes) throws JAXBException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        setAttributes(attributes, platformEntity);
        JAXBContext context = JAXBContext.newInstance("generated");
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        marshaller.marshal(uddFile, System.out);
    }

    public void addFlexField(Map<String, Object> mappings) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        FlexFieldType flexField = factory.createFlexFieldType();
        setAttributes(mappings, flexField);
        platformEntity.getFlexFieldOrFlexFieldListOrFlexIndex().add(flexField);
    }

    public void addStaticEnum(Map<String, Object> mappings, List<String> items) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        StaticEnumType staticEnum = factory.createStaticEnumType();
        setAttributes(mappings, staticEnum);
        IntStream.range(0, items.size()).forEach (i -> {
            StaticEnumType.EnumItem  item = factory.createStaticEnumTypeEnumItem();
            item.setName(items.get(i));
            item.setDbValue(Integer.toString(i));
            item.setApiValue(items.get(i));
            staticEnum.getEnumItem().add(item);
        });
        JAXBElement<StaticEnumType> staticEnum1 = factory.createAbstractFeatureTypeStaticEnum(staticEnum);
        fragment.getStaticEnumOrDomainSetOrDomainSetExtension().add(staticEnum1);
    }

}
