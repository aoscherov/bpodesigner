package sample;

public class UddAttributes {
	public static final String KEY_PREFIX = "KeyPrefix";
	public static final String OWNER = "Owner";
	public static final String NAME = "Name";
	public static final String MIN_API_VERSION = "MinApiVersion";
	public static final String JAVA_PKG_ROOT = "JavaPackageRoot";
	public static final String EDIT_ACCESS = "EditAccess";
	
	// flex fields
	public static final String SLOT = "Slot";
	public static final String COLUMN_TYPE = "ColumnType";
	public static final String MAX_LENGTH = "MaxLength";
	public static final String LABEL_SECTION = "LabelSection";
	public static final String OUTPUT_SQL = "OutputAsSql";
}
