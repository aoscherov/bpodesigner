package sample;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import process.XMLGenerator;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {
	
	public static final int LEFT_MARGIN = 20;
	public static final int LEFT_MARGIN_INDENT = 40;
	public static final int LEFT_MARGIN_TF = 200;
	
	public static final String TEXT = "TEXT";
	public static final String STATICENUM = "STATICENUM";
	
	
	Map<String, Object> entityAttributes = new HashMap<>();
	XMLGenerator generator = new XMLGenerator();
	
    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("BPO Designer");
        primaryStage.setWidth(400);
        primaryStage.setHeight(450);
        Group platformEntity = new Group();
        
        ObservableList<Node> entityChildren = platformEntity.getChildren();
        
        // Platform entity attributes label
        Label entityAttrLabel = new Label("Platform Entity Attributes");
        entityAttrLabel.setLayoutX(LEFT_MARGIN);
        entityAttrLabel.setLayoutY(20);
        platformEntity.getChildren().add(entityAttrLabel);
        
        // Platform entity attributes text fields and boxes
        // name
        // keyPrefix
        LabelTextFieldPair namePair = new LabelTextFieldPair(UddAttributes.NAME, 50);
        addPairToGroupChildren(namePair, entityChildren);
        
        // keyPrefix
        LabelTextFieldPair keyPrefixPair = new LabelTextFieldPair(UddAttributes.KEY_PREFIX, 80);
        addPairToGroupChildren(keyPrefixPair, entityChildren);
        
        // owner
        LabelTextFieldPair ownerPair = new LabelTextFieldPair(UddAttributes.OWNER, 110);
        addPairToGroupChildren(ownerPair, entityChildren);
        
        // minApiVersion
        LabelTextFieldPair minApiPair = new LabelTextFieldPair(UddAttributes.MIN_API_VERSION, 140);
        addPairToGroupChildren(minApiPair, entityChildren);
        
        // javaPackageRoot
        LabelTextFieldPair javaPkgPair = new LabelTextFieldPair(UddAttributes.JAVA_PKG_ROOT, 170);
        addPairToGroupChildren(javaPkgPair, entityChildren);
        
        // editAccess
        LabelTextFieldPair editAccessPair = new LabelTextFieldPair(UddAttributes.EDIT_ACCESS, 200);
        addPairToGroupChildren(editAccessPair, entityChildren);
        
        
        // Flex fields
        Label flexFieldsLabel = new Label("Flex fields");
        flexFieldsLabel.setLayoutX(LEFT_MARGIN);
        flexFieldsLabel.setLayoutY(260);
        platformEntity.getChildren().add(flexFieldsLabel);
        
        // Field type selector
        ComboBox<String> flexComboBox = new ComboBox<>();
        flexComboBox.getItems().addAll(TEXT, STATICENUM);
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.add(flexComboBox, 1, 0);
        grid.setLayoutX(30);
        grid.setLayoutY(285);
        entityChildren.add(grid);
        
        Button createFlexFieldButton = new Button ("Create flexField");
        createFlexFieldButton.setLayoutX(230);
        createFlexFieldButton.setLayoutY(290);
        createFlexFieldButton.setOnAction(actionEvent -> {
        	String type = flexComboBox.getValue();
			if (TEXT.equals(type)) {
				getFlexFieldInput(generator, type);
			} else { // static enum
				getStaticEnumInput(generator, type);
			}
		});
        entityChildren.add(createFlexFieldButton);
        
        Button generateButton = new Button("Generate UDD XML");
        generateButton.setLayoutX(130);
        generateButton.setLayoutY(370);
        generateButton.setOnAction(actionEvent -> {
        	entityAttributes.put(UddAttributes.KEY_PREFIX, keyPrefixPair.getTextField().getText());
        	entityAttributes.put(UddAttributes.OWNER, ownerPair.getTextField().getText());
        	entityAttributes.put(UddAttributes.MIN_API_VERSION, new Integer(minApiPair.getTextField().getText()));
        	entityAttributes.put(UddAttributes.JAVA_PKG_ROOT, javaPkgPair.getTextField().getText());
        	entityAttributes.put(UddAttributes.EDIT_ACCESS, editAccessPair.getTextField().getText());
        	try {
				generator.generate(entityAttributes);
				System.exit(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
        });
        
        entityChildren.add(generateButton);
        
        primaryStage.setScene(new Scene(platformEntity, 0, 0));
        primaryStage.show();
    }
    
    private Map<String, Object> getStaticEnumInput(XMLGenerator generator, String columnType) {
    	Stage flexStage = new Stage();
    	Map<String, Object> fields = new HashMap<>();
    	Map<String, Object> enumFields = new HashMap<>();
    	flexStage.setWidth(400);
    	flexStage.setHeight(350);
    	flexStage.setTitle("Add a new static enum");
    	Group flexField = new Group();
    	ObservableList<Node> flexChildren = flexField.getChildren();
    	
    	// Flex fields header
    	Label flexFieldsLabel = new Label("Static enum attributes");
        flexFieldsLabel.setLayoutX(LEFT_MARGIN);
        flexFieldsLabel.setLayoutY(10);
        flexChildren.add(flexFieldsLabel);
    	
    	// Flex fields attributes text fields and boxes
        LabelTextFieldPair namePair = new LabelTextFieldPair(UddAttributes.NAME, 40);
        addPairToGroupChildren(namePair, flexChildren);
        
        LabelTextFieldPair slotPair = new LabelTextFieldPair(UddAttributes.SLOT, 70);
        addPairToGroupChildren(slotPair, flexChildren);
        
        LabelTextFieldPair nameEnumPair = new LabelTextFieldPair(UddAttributes.NAME, 100);
        addPairToGroupChildren(nameEnumPair, flexChildren);

        LabelTextFieldPair labelSectionPair = new LabelTextFieldPair(UddAttributes.LABEL_SECTION, 130);
        addPairToGroupChildren(labelSectionPair, flexChildren);
        
        LabelTextFieldPair outputSqlPair = new LabelTextFieldPair(UddAttributes.OUTPUT_SQL, 160);
        addPairToGroupChildren(outputSqlPair, flexChildren);
        
        Label enumItemsLabel = new Label("Enum item names\n(separated by commas)");
        enumItemsLabel.setLayoutX(LEFT_MARGIN_INDENT);
        enumItemsLabel.setLayoutY(190);
        flexChildren.add(enumItemsLabel);
        TextArea enumItems = new TextArea();
        enumItems.setPrefRowCount(3);
        enumItems.setPrefColumnCount(100);
        enumItems.setWrapText(true);
        enumItems.setPrefWidth(150);
        enumItems.setLayoutX(LEFT_MARGIN_TF);
        enumItems.setLayoutY(190);
        flexChildren.add(enumItems);
        
        // button to add
        Button addFlexFieldButton = new Button("Add staticEnum");
        addFlexFieldButton.setLayoutX(150);
        addFlexFieldButton.setLayoutY(270);
        addFlexFieldButton.setOnAction(actionEvent -> {
        	fields.put(UddAttributes.NAME, namePair.getTextField().getText());
        	fields.put(UddAttributes.SLOT, slotPair.getTextField().getText());
        	fields.put(UddAttributes.COLUMN_TYPE, columnType);
            enumFields.put(UddAttributes.NAME, nameEnumPair.getTextField().getText());
        	enumFields.put(UddAttributes.LABEL_SECTION, labelSectionPair.getTextField().getText());
        	enumFields.put(UddAttributes.OUTPUT_SQL, Boolean.valueOf(outputSqlPair.getTextField().getText()));
        	try {
        		generator.addFlexField(fields);
				generator.addStaticEnum(enumFields, Arrays.asList(enumItems.getText().split(",[ ]*")));
			} catch (Exception e) {
				e.printStackTrace();
			}
        	flexStage.close();
        });
        flexChildren.add(addFlexFieldButton);
        
    	flexStage.setScene(new Scene(flexField, 0, 0));
    	flexStage.show();
    	return fields;
    }
    
    private Map<String, Object> getFlexFieldInput(XMLGenerator generator, String columnType) {
    	Stage flexStage = new Stage();
    	Map<String, Object> fields = new HashMap<>();
    	flexStage.setWidth(400);
    	flexStage.setHeight(230);
    	flexStage.setTitle("Add a new flex field");
    	Group flexField = new Group();
    	ObservableList<Node> flexChildren = flexField.getChildren();
    	
    	// Flex fields header
    	Label flexFieldsLabel = new Label("Flex field attributes");
        flexFieldsLabel.setLayoutX(LEFT_MARGIN);
        flexFieldsLabel.setLayoutY(10);
        flexChildren.add(flexFieldsLabel);
    	
    	// Flex fields attributes text fields and boxes
        LabelTextFieldPair namePair = new LabelTextFieldPair(UddAttributes.NAME, 40);
        addPairToGroupChildren(namePair, flexChildren);
        
        LabelTextFieldPair slotPair = new LabelTextFieldPair(UddAttributes.SLOT, 70);
        addPairToGroupChildren(slotPair, flexChildren);
        
        LabelTextFieldPair maxLengthPair = new LabelTextFieldPair(UddAttributes.MAX_LENGTH, 100);
        addPairToGroupChildren(maxLengthPair, flexChildren);
    	
        // button to add
        Button addFlexFieldButton = new Button("Add flexField");
        addFlexFieldButton.setLayoutX(150);
        addFlexFieldButton.setLayoutY(150);
        addFlexFieldButton.setOnAction(actionEvent -> {
        	fields.put(UddAttributes.NAME, namePair.getTextField().getText());
        	fields.put(UddAttributes.SLOT, slotPair.getTextField().getText());
        	fields.put(UddAttributes.COLUMN_TYPE, columnType);
        	fields.put(UddAttributes.MAX_LENGTH, new Integer(maxLengthPair.getTextField().getText()));
        	try {
				generator.addFlexField(fields);
			} catch (Exception e) {
				e.printStackTrace();
			}
        	flexStage.close();
        });
        flexChildren.add(addFlexFieldButton);
        
    	flexStage.setScene(new Scene(flexField, 0, 0));
    	flexStage.show();
    	return fields;
    }
    
    private void addPairToGroupChildren(LabelTextFieldPair pair, ObservableList<Node> entityChildren) {
    	entityChildren.add(pair.getLabel());
    	entityChildren.add(pair.getTextField());
    }
    
    public class LabelTextFieldPair {
    	Label label;
    	TextField textField;
    	
    	public LabelTextFieldPair(String labelName, int layoutY) {
    		label = new Label(labelName + ": ");
    		textField = new TextField();
    		label.setLayoutX(LEFT_MARGIN_INDENT);
    		textField.setLayoutX(LEFT_MARGIN_TF);
    		label.setLayoutY(layoutY);
    		textField.setLayoutY(layoutY);
    	}
    	
    	public Label getLabel() {
    		return label;
    	}
    	
    	public TextField getTextField() {
    		return textField;
    	}
    }


    public static void main(String[] args) {
        launch(args);
    }
}