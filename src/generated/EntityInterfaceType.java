//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 Defines an entity interface.  Entity interfaces may be used in the platform and bound to one or more
 *                 implementing entities via organization-specific configuration.
 *             
 * 
 * <p>Java class for entityInterfaceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entityInterfaceType">
 *   &lt;complexContent>
 *     &lt;extension base="{}baseAbstractFlexEntity">
 *       &lt;sequence>
 *         &lt;element name="field" type="{}interfaceFieldType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="layout" type="{}layoutType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="standardImplementation" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="hasCurrencyCodeField" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="genView" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="extends" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="apiQueryAsUnionOfImplementations" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entityInterfaceType", propOrder = {
    "field",
    "layout"
})
public class EntityInterfaceType
    extends BaseAbstractFlexEntity
{

    protected List<InterfaceFieldType> field;
    protected List<LayoutType> layout;
    @XmlAttribute(name = "standardImplementation")
    protected String standardImplementation;
    @XmlAttribute(name = "hasCurrencyCodeField")
    protected Boolean hasCurrencyCodeField;
    @XmlAttribute(name = "genView")
    protected Boolean genView;
    @XmlAttribute(name = "extends")
    protected String _extends;
    @XmlAttribute(name = "apiQueryAsUnionOfImplementations")
    protected Boolean apiQueryAsUnionOfImplementations;

    /**
     * Gets the value of the field property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the field property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterfaceFieldType }
     * 
     * 
     */
    public List<InterfaceFieldType> getField() {
        if (field == null) {
            field = new ArrayList<InterfaceFieldType>();
        }
        return this.field;
    }

    /**
     * Gets the value of the layout property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the layout property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLayout().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LayoutType }
     * 
     * 
     */
    public List<LayoutType> getLayout() {
        if (layout == null) {
            layout = new ArrayList<LayoutType>();
        }
        return this.layout;
    }

    /**
     * Gets the value of the standardImplementation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardImplementation() {
        return standardImplementation;
    }

    /**
     * Sets the value of the standardImplementation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardImplementation(String value) {
        this.standardImplementation = value;
    }

    /**
     * Gets the value of the hasCurrencyCodeField property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasCurrencyCodeField() {
        return hasCurrencyCodeField;
    }

    /**
     * Sets the value of the hasCurrencyCodeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasCurrencyCodeField(Boolean value) {
        this.hasCurrencyCodeField = value;
    }

    /**
     * Gets the value of the genView property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenView() {
        return genView;
    }

    /**
     * Sets the value of the genView property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenView(Boolean value) {
        this.genView = value;
    }

    /**
     * Gets the value of the extends property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtends() {
        return _extends;
    }

    /**
     * Sets the value of the extends property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtends(String value) {
        this._extends = value;
    }

    /**
     * Gets the value of the apiQueryAsUnionOfImplementations property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApiQueryAsUnionOfImplementations() {
        return apiQueryAsUnionOfImplementations;
    }

    /**
     * Sets the value of the apiQueryAsUnionOfImplementations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApiQueryAsUnionOfImplementations(Boolean value) {
        this.apiQueryAsUnionOfImplementations = value;
    }

}
