//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entityUnionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entityUnionType">
 *   &lt;complexContent>
 *     &lt;extension base="{}abstractFeatureMemberType">
 *       &lt;sequence>
 *         &lt;element name="field" type="{}fieldType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="javaPackageRoot" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="motifName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="apiAccess" type="{}accessExpression" />
 *       &lt;attribute name="userAccess" type="{}accessExpression" />
 *       &lt;attribute name="union" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="keyPrefix" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="owner" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="hasACListView" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isStorageCounted" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isApproxCounted" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="supportedQueryScopes" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="supportedPageTypes" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isRenamable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="hasNonNullKeyPrefix" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isSearchLayoutable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="deleteMethod" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="plsqlDeletePrepHook" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="plsqlDeleteExecHook" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="plsqlUndeleteHook" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="plsqlPhysDeleteHook" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="featureGroups" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entityUnionType", propOrder = {
    "field"
})
public class EntityUnionType
    extends AbstractFeatureMemberType
{

    @XmlElement(required = true)
    protected List<FieldType> field;
    @XmlAttribute(name = "javaPackageRoot")
    protected String javaPackageRoot;
    @XmlAttribute(name = "motifName")
    protected String motifName;
    @XmlAttribute(name = "apiAccess")
    protected String apiAccess;
    @XmlAttribute(name = "userAccess")
    protected String userAccess;
    @XmlAttribute(name = "union", required = true)
    protected String union;
    @XmlAttribute(name = "keyPrefix")
    protected String keyPrefix;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "owner")
    protected String owner;
    @XmlAttribute(name = "hasACListView")
    protected String hasACListView;
    @XmlAttribute(name = "isStorageCounted")
    protected String isStorageCounted;
    @XmlAttribute(name = "isApproxCounted")
    protected String isApproxCounted;
    @XmlAttribute(name = "supportedQueryScopes")
    protected String supportedQueryScopes;
    @XmlAttribute(name = "supportedPageTypes")
    protected String supportedPageTypes;
    @XmlAttribute(name = "isRenamable")
    protected Boolean isRenamable;
    @XmlAttribute(name = "hasNonNullKeyPrefix")
    protected Boolean hasNonNullKeyPrefix;
    @XmlAttribute(name = "isSearchLayoutable")
    protected Boolean isSearchLayoutable;
    @XmlAttribute(name = "deleteMethod")
    protected String deleteMethod;
    @XmlAttribute(name = "plsqlDeletePrepHook")
    protected String plsqlDeletePrepHook;
    @XmlAttribute(name = "plsqlDeleteExecHook")
    protected String plsqlDeleteExecHook;
    @XmlAttribute(name = "plsqlUndeleteHook")
    protected String plsqlUndeleteHook;
    @XmlAttribute(name = "plsqlPhysDeleteHook")
    protected String plsqlPhysDeleteHook;
    @XmlAttribute(name = "featureGroups")
    protected String featureGroups;

    /**
     * Gets the value of the field property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the field property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldType }
     * 
     * 
     */
    public List<FieldType> getField() {
        if (field == null) {
            field = new ArrayList<FieldType>();
        }
        return this.field;
    }

    /**
     * Gets the value of the javaPackageRoot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJavaPackageRoot() {
        return javaPackageRoot;
    }

    /**
     * Sets the value of the javaPackageRoot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJavaPackageRoot(String value) {
        this.javaPackageRoot = value;
    }

    /**
     * Gets the value of the motifName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotifName() {
        return motifName;
    }

    /**
     * Sets the value of the motifName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotifName(String value) {
        this.motifName = value;
    }

    /**
     * Gets the value of the apiAccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiAccess() {
        return apiAccess;
    }

    /**
     * Sets the value of the apiAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiAccess(String value) {
        this.apiAccess = value;
    }

    /**
     * Gets the value of the userAccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserAccess() {
        return userAccess;
    }

    /**
     * Sets the value of the userAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserAccess(String value) {
        this.userAccess = value;
    }

    /**
     * Gets the value of the union property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnion() {
        return union;
    }

    /**
     * Sets the value of the union property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnion(String value) {
        this.union = value;
    }

    /**
     * Gets the value of the keyPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyPrefix() {
        return keyPrefix;
    }

    /**
     * Sets the value of the keyPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyPrefix(String value) {
        this.keyPrefix = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the hasACListView property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasACListView() {
        return hasACListView;
    }

    /**
     * Sets the value of the hasACListView property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasACListView(String value) {
        this.hasACListView = value;
    }

    /**
     * Gets the value of the isStorageCounted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStorageCounted() {
        return isStorageCounted;
    }

    /**
     * Sets the value of the isStorageCounted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStorageCounted(String value) {
        this.isStorageCounted = value;
    }

    /**
     * Gets the value of the isApproxCounted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsApproxCounted() {
        return isApproxCounted;
    }

    /**
     * Sets the value of the isApproxCounted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsApproxCounted(String value) {
        this.isApproxCounted = value;
    }

    /**
     * Gets the value of the supportedQueryScopes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportedQueryScopes() {
        return supportedQueryScopes;
    }

    /**
     * Sets the value of the supportedQueryScopes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportedQueryScopes(String value) {
        this.supportedQueryScopes = value;
    }

    /**
     * Gets the value of the supportedPageTypes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportedPageTypes() {
        return supportedPageTypes;
    }

    /**
     * Sets the value of the supportedPageTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportedPageTypes(String value) {
        this.supportedPageTypes = value;
    }

    /**
     * Gets the value of the isRenamable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRenamable() {
        return isRenamable;
    }

    /**
     * Sets the value of the isRenamable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRenamable(Boolean value) {
        this.isRenamable = value;
    }

    /**
     * Gets the value of the hasNonNullKeyPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasNonNullKeyPrefix() {
        return hasNonNullKeyPrefix;
    }

    /**
     * Sets the value of the hasNonNullKeyPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasNonNullKeyPrefix(Boolean value) {
        this.hasNonNullKeyPrefix = value;
    }

    /**
     * Gets the value of the isSearchLayoutable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSearchLayoutable() {
        return isSearchLayoutable;
    }

    /**
     * Sets the value of the isSearchLayoutable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSearchLayoutable(Boolean value) {
        this.isSearchLayoutable = value;
    }

    /**
     * Gets the value of the deleteMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteMethod() {
        return deleteMethod;
    }

    /**
     * Sets the value of the deleteMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteMethod(String value) {
        this.deleteMethod = value;
    }

    /**
     * Gets the value of the plsqlDeletePrepHook property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlsqlDeletePrepHook() {
        return plsqlDeletePrepHook;
    }

    /**
     * Sets the value of the plsqlDeletePrepHook property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlsqlDeletePrepHook(String value) {
        this.plsqlDeletePrepHook = value;
    }

    /**
     * Gets the value of the plsqlDeleteExecHook property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlsqlDeleteExecHook() {
        return plsqlDeleteExecHook;
    }

    /**
     * Sets the value of the plsqlDeleteExecHook property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlsqlDeleteExecHook(String value) {
        this.plsqlDeleteExecHook = value;
    }

    /**
     * Gets the value of the plsqlUndeleteHook property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlsqlUndeleteHook() {
        return plsqlUndeleteHook;
    }

    /**
     * Sets the value of the plsqlUndeleteHook property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlsqlUndeleteHook(String value) {
        this.plsqlUndeleteHook = value;
    }

    /**
     * Gets the value of the plsqlPhysDeleteHook property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlsqlPhysDeleteHook() {
        return plsqlPhysDeleteHook;
    }

    /**
     * Sets the value of the plsqlPhysDeleteHook property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlsqlPhysDeleteHook(String value) {
        this.plsqlPhysDeleteHook = value;
    }

    /**
     * Gets the value of the featureGroups property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureGroups() {
        return featureGroups;
    }

    /**
     * Sets the value of the featureGroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureGroups(String value) {
        this.featureGroups = value;
    }

}
