//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for baseConcreteFlexEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="baseConcreteFlexEntity">
 *   &lt;complexContent>
 *     &lt;extension base="{}baseFlexEntity">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="sqlSource" type="{}sqlSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *       &lt;attribute name="genView" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="sandboxCopyLevel" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "baseConcreteFlexEntity", propOrder = {
    "sqlSource"
})
@XmlSeeAlso({
    PlatformEntityType.class,
    MetricsEntityType.class,
    SetupEntityType.class
})
public class BaseConcreteFlexEntity
    extends BaseFlexEntity
{

    protected List<SqlSourceType> sqlSource;
    @XmlAttribute(name = "genView")
    protected Boolean genView;
    @XmlAttribute(name = "sandboxCopyLevel")
    protected String sandboxCopyLevel;

    /**
     * Gets the value of the sqlSource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sqlSource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSqlSource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SqlSourceType }
     * 
     * 
     */
    public List<SqlSourceType> getSqlSource() {
        if (sqlSource == null) {
            sqlSource = new ArrayList<SqlSourceType>();
        }
        return this.sqlSource;
    }

    /**
     * Gets the value of the genView property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGenView() {
        if (genView == null) {
            return false;
        } else {
            return genView;
        }
    }

    /**
     * Sets the value of the genView property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenView(Boolean value) {
        this.genView = value;
    }

    /**
     * Gets the value of the sandboxCopyLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSandboxCopyLevel() {
        return sandboxCopyLevel;
    }

    /**
     * Sets the value of the sandboxCopyLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSandboxCopyLevel(String value) {
        this.sandboxCopyLevel = value;
    }

}
