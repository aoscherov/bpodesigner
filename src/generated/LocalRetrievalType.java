//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for localRetrievalType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="localRetrievalType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="IMMUTABLE"/>
 *     &lt;enumeration value="CLONE_ON_RETURN"/>
 *     &lt;enumeration value="DESERIALIZE_ON_RETURN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "localRetrievalType")
@XmlEnum
public enum LocalRetrievalType {

    IMMUTABLE,
    CLONE_ON_RETURN,
    DESERIALIZE_ON_RETURN;

    public String value() {
        return name();
    }

    public static LocalRetrievalType fromValue(String v) {
        return valueOf(v);
    }

}
