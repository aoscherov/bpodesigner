//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DmlTypeWithNonSetupType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DmlTypeWithNonSetupType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="insert"/>
 *     &lt;enumeration value="update"/>
 *     &lt;enumeration value="upsert"/>
 *     &lt;enumeration value="any"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DmlTypeWithNonSetupType")
@XmlEnum
public enum DmlTypeWithNonSetupType {

    @XmlEnumValue("insert")
    INSERT("insert"),
    @XmlEnumValue("update")
    UPDATE("update"),
    @XmlEnumValue("upsert")
    UPSERT("upsert"),
    @XmlEnumValue("any")
    ANY("any");
    private final String value;

    DmlTypeWithNonSetupType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DmlTypeWithNonSetupType fromValue(String v) {
        for (DmlTypeWithNonSetupType c: DmlTypeWithNonSetupType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
