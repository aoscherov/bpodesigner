//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for orgPermTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="orgPermTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="provisioned"/>
 *     &lt;enumeration value="neverProvisioned"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "orgPermTypeType")
@XmlEnum
public enum OrgPermTypeType {


    /**
     * 
     *                         Anything you want the provisioning framework to enable/disable based on the edition the user
     *                         signed up. You cannot edit a perm of this type in black tab. Only provisionable perms are
     *                         touched by the L&P framework during provisioning.
     *                     
     * 
     */
    @XmlEnumValue("provisioned")
    PROVISIONED("provisioned"),

    /**
     * 
     *                         Perms of this type are enabled/disabled via black tab page by support/qa and people with
     *                         sufficient privileges.
     *                     
     * 
     */
    @XmlEnumValue("neverProvisioned")
    NEVER_PROVISIONED("neverProvisioned");
    private final String value;

    OrgPermTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrgPermTypeType fromValue(String v) {
        for (OrgPermTypeType c: OrgPermTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
