//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for virtualEntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="virtualEntityType">
 *   &lt;complexContent>
 *     &lt;extension base="{}baseAbstractFlexEntity">
 *       &lt;sequence>
 *         &lt;element name="virtualField" type="{}virtualFieldType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="layout" type="{}layoutType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="dataSource" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="externalAlias" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="hasCurrencyCodeField" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="hasExternalIdField" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="hasOrganizationId" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "virtualEntityType", propOrder = {
    "virtualField",
    "layout"
})
public class VirtualEntityType
    extends BaseAbstractFlexEntity
{

    protected List<VirtualFieldType> virtualField;
    protected List<LayoutType> layout;
    @XmlAttribute(name = "dataSource")
    protected String dataSource;
    @XmlAttribute(name = "externalAlias")
    protected String externalAlias;
    @XmlAttribute(name = "hasCurrencyCodeField")
    protected Boolean hasCurrencyCodeField;
    @XmlAttribute(name = "hasExternalIdField")
    protected Boolean hasExternalIdField;
    @XmlAttribute(name = "hasOrganizationId")
    protected Boolean hasOrganizationId;

    /**
     * Gets the value of the virtualField property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the virtualField property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVirtualField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VirtualFieldType }
     * 
     * 
     */
    public List<VirtualFieldType> getVirtualField() {
        if (virtualField == null) {
            virtualField = new ArrayList<VirtualFieldType>();
        }
        return this.virtualField;
    }

    /**
     * Gets the value of the layout property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the layout property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLayout().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LayoutType }
     * 
     * 
     */
    public List<LayoutType> getLayout() {
        if (layout == null) {
            layout = new ArrayList<LayoutType>();
        }
        return this.layout;
    }

    /**
     * Gets the value of the dataSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataSource() {
        return dataSource;
    }

    /**
     * Sets the value of the dataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataSource(String value) {
        this.dataSource = value;
    }

    /**
     * Gets the value of the externalAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalAlias() {
        return externalAlias;
    }

    /**
     * Sets the value of the externalAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalAlias(String value) {
        this.externalAlias = value;
    }

    /**
     * Gets the value of the hasCurrencyCodeField property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasCurrencyCodeField() {
        return hasCurrencyCodeField;
    }

    /**
     * Sets the value of the hasCurrencyCodeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasCurrencyCodeField(Boolean value) {
        this.hasCurrencyCodeField = value;
    }

    /**
     * Gets the value of the hasExternalIdField property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasExternalIdField() {
        return hasExternalIdField;
    }

    /**
     * Sets the value of the hasExternalIdField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasExternalIdField(Boolean value) {
        this.hasExternalIdField = value;
    }

    /**
     * Gets the value of the hasOrganizationId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasOrganizationId() {
        return hasOrganizationId;
    }

    /**
     * Sets the value of the hasOrganizationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasOrganizationId(Boolean value) {
        this.hasOrganizationId = value;
    }

}
