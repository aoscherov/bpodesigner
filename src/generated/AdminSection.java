//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adminSection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="adminSection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="config"/>
 *     &lt;enumeration value="manual"/>
 *     &lt;enumeration value="email"/>
 *     &lt;enumeration value="advanced"/>
 *     &lt;enumeration value="other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "adminSection")
@XmlEnum
public enum AdminSection {

    @XmlEnumValue("config")
    CONFIG("config"),
    @XmlEnumValue("manual")
    MANUAL("manual"),
    @XmlEnumValue("email")
    EMAIL("email"),
    @XmlEnumValue("advanced")
    ADVANCED("advanced"),
    @XmlEnumValue("other")
    OTHER("other");
    private final String value;

    AdminSection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdminSection fromValue(String v) {
        for (AdminSection c: AdminSection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
