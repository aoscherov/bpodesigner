//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.27 at 09:22:28 AM PDT 
//


package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for orgSettingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="orgSettingType">
 *   &lt;complexContent>
 *     &lt;extension base="{}booleanSettingType">
 *       &lt;attribute name="useDefaultForCustomSignup" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="adminSection" type="{}adminSection" />
 *       &lt;attribute name="layoutSection" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="disableable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="enableOrgHistory" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="orgHistoryField" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="internalOrgHistoryField" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="filterable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="filterLabel" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="adminLevel" type="{}adminLevel" />
 *       &lt;attribute name="sysAdminEditable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="visibleInBlacktab" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="reEnableable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "orgSettingType")
@XmlSeeAlso({
    OrgPermType.class,
    OrgPrefType.class
})
public abstract class OrgSettingType
    extends BooleanSettingType
{

    @XmlAttribute(name = "useDefaultForCustomSignup")
    protected Boolean useDefaultForCustomSignup;
    @XmlAttribute(name = "required")
    protected String required;
    @XmlAttribute(name = "adminSection")
    protected AdminSection adminSection;
    @XmlAttribute(name = "layoutSection")
    protected String layoutSection;
    @XmlAttribute(name = "disableable")
    protected Boolean disableable;
    @XmlAttribute(name = "enableOrgHistory")
    protected Boolean enableOrgHistory;
    @XmlAttribute(name = "orgHistoryField")
    protected String orgHistoryField;
    @XmlAttribute(name = "internalOrgHistoryField")
    protected String internalOrgHistoryField;
    @XmlAttribute(name = "filterable")
    protected Boolean filterable;
    @XmlAttribute(name = "filterLabel")
    protected String filterLabel;
    @XmlAttribute(name = "adminLevel")
    protected AdminLevel adminLevel;
    @XmlAttribute(name = "sysAdminEditable")
    protected Boolean sysAdminEditable;
    @XmlAttribute(name = "visibleInBlacktab")
    protected Boolean visibleInBlacktab;
    @XmlAttribute(name = "reEnableable")
    protected Boolean reEnableable;

    /**
     * Gets the value of the useDefaultForCustomSignup property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDefaultForCustomSignup() {
        return useDefaultForCustomSignup;
    }

    /**
     * Sets the value of the useDefaultForCustomSignup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDefaultForCustomSignup(Boolean value) {
        this.useDefaultForCustomSignup = value;
    }

    /**
     * Gets the value of the required property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequired() {
        return required;
    }

    /**
     * Sets the value of the required property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequired(String value) {
        this.required = value;
    }

    /**
     * Gets the value of the adminSection property.
     * 
     * @return
     *     possible object is
     *     {@link AdminSection }
     *     
     */
    public AdminSection getAdminSection() {
        return adminSection;
    }

    /**
     * Sets the value of the adminSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdminSection }
     *     
     */
    public void setAdminSection(AdminSection value) {
        this.adminSection = value;
    }

    /**
     * Gets the value of the layoutSection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLayoutSection() {
        return layoutSection;
    }

    /**
     * Sets the value of the layoutSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLayoutSection(String value) {
        this.layoutSection = value;
    }

    /**
     * Gets the value of the disableable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableable() {
        return disableable;
    }

    /**
     * Sets the value of the disableable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableable(Boolean value) {
        this.disableable = value;
    }

    /**
     * Gets the value of the enableOrgHistory property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableOrgHistory() {
        return enableOrgHistory;
    }

    /**
     * Sets the value of the enableOrgHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableOrgHistory(Boolean value) {
        this.enableOrgHistory = value;
    }

    /**
     * Gets the value of the orgHistoryField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgHistoryField() {
        return orgHistoryField;
    }

    /**
     * Sets the value of the orgHistoryField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgHistoryField(String value) {
        this.orgHistoryField = value;
    }

    /**
     * Gets the value of the internalOrgHistoryField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalOrgHistoryField() {
        return internalOrgHistoryField;
    }

    /**
     * Sets the value of the internalOrgHistoryField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalOrgHistoryField(String value) {
        this.internalOrgHistoryField = value;
    }

    /**
     * Gets the value of the filterable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFilterable() {
        return filterable;
    }

    /**
     * Sets the value of the filterable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFilterable(Boolean value) {
        this.filterable = value;
    }

    /**
     * Gets the value of the filterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterLabel() {
        return filterLabel;
    }

    /**
     * Sets the value of the filterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterLabel(String value) {
        this.filterLabel = value;
    }

    /**
     * Gets the value of the adminLevel property.
     * 
     * @return
     *     possible object is
     *     {@link AdminLevel }
     *     
     */
    public AdminLevel getAdminLevel() {
        return adminLevel;
    }

    /**
     * Sets the value of the adminLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdminLevel }
     *     
     */
    public void setAdminLevel(AdminLevel value) {
        this.adminLevel = value;
    }

    /**
     * Gets the value of the sysAdminEditable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSysAdminEditable() {
        return sysAdminEditable;
    }

    /**
     * Sets the value of the sysAdminEditable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSysAdminEditable(Boolean value) {
        this.sysAdminEditable = value;
    }

    /**
     * Gets the value of the visibleInBlacktab property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVisibleInBlacktab() {
        return visibleInBlacktab;
    }

    /**
     * Sets the value of the visibleInBlacktab property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisibleInBlacktab(Boolean value) {
        this.visibleInBlacktab = value;
    }

    /**
     * Gets the value of the reEnableable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReEnableable() {
        return reEnableable;
    }

    /**
     * Sets the value of the reEnableable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReEnableable(Boolean value) {
        this.reEnableable = value;
    }

}
